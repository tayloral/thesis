%\chapter{Autonomous Mapping}
%\label{chapMapping}\pagestyle{chapterpage}
%\setlength{\epigraphwidth}{0.63\textwidth}
%\epigraph{Get your facts first, then you can distort them as you please.}{Mark Twain}\glsresetall\noindent
\section{Autonomous Mapping}\label{chapMapping}
This chapter will introduce on-line \glspl{itm}.  As \gls{ptl} operates on-line, it is not practical to have \glspl{itm} provided off-line.  The combinations of agents can not be known prior to the execution of a system.  This necessitates the production of \glspl{itm} on-line.  Learning \glspl{itm} is necessary for applying \gls{ptl} between heterogeneous agents.
\subsection{Introduction}
%todo SECTION intorduction make clear is hard and restate all that has bean said upto this point
Transferring information from one agent to another relies on the fact that knowledge can be made mutuality intelligible.  If the agents are homogeneous, then no extra processing of knowledge is needed.  If they are in some way non-homogeneous, then knowledge must be translated so that it can be understood by the target.  The form of this translation depends on the difference between the agents in question.  The knowledge is translated by a function called an \gls{itm}.  $\chi_{source\rightarrow target}$ denotes the \gls{itm} from source to target.  Generally the more different the agents, the greater the risk of negative transfer.  This can be compounded by a poor \gls{itm}.  If there is good, useful knowledge to share it must be transferred to the correct state to provide any benefit at the target agent.  As discussed in Chapter~\ref{chapRelated}, the production of \glspl{itm} has typically been done by designers or using computationally intensive methods both which are not scalable.  It is also an off-line process.  While mappings calculated off-line can be effective, they must know what agents will be present in a system before it is run.  They can not adapt to change in the environment at run time without further design work.  To allow these mappings to be used with \gls{ptl}, they must be calculated on-line.  As \gls{ptl} loses the initial performance boost that \gls{tl} gets due to a starting lack of information, there is time available to produce a mapping on-line without compromising performance.  \glspl{itm} can be used to transfer any type of information (value function, model, policies, etc.), but \gls{ptl} focuses on value function information and so will this chapter and by extension the \glspl{itm} produced.

As a single \gls{itm} is a complicated function that will be produced on-line, some clarification of terminology is required.
\begin{itemize}
\item \textbf{\gls{itm}} is the entire mapping.  It can translate any value at the source to the target or deliberately stop translation (if a state has no match).  In Figure~\ref{4imgTerms}, it is the set of red lines.
\item \textbf{Strand} a single state-action pair's mapping to the target.  An \gls{itm} is composed of many strands.  In Figure~\ref{4imgTerms} the red lines are strands in an \gls{itm}, the black strands are potential ones but currently unused.
\item \textbf{Feedback Tuple} a piece of feedback for a particular strand of the mapping.
\item \textbf{Effective} is when a strand is properly matched and allows useful information to be exchanged.
\item A \textbf{Correspondence} is when there is potential for a good strand between two state-action pairs.
\end{itemize}
\begin{figure}[hbt!]
\centering
\includegraphics[width=\linewidth]{figures/chap4Terms}
\caption[\acrlong{itm} Terminology]{\gls{itm} Terminology.}
\label{4imgTerms}
\end{figure}
Figure~\ref{4imgTerms} shows an example \gls{itm} (in red) between two tasks.  It also shows all potential strands.  In this small example, the two tasks have state-spaces of $4$ and $3$ state-action pairs, this leads to $12$ potential strands of which $4$ are selected.  Between any two tasks there will be $\left\vert{state\mbox{-}space_{source}}\right\vert*\left\vert{state\mbox{-}space_{target}}\right\vert$ potential strands.  In this example, the size of the source's state-space and the \gls{itm} are the same.  This is not necessarily true, as multiple strands can lead to or from state-action pairs.  Not all state-action pairs will have correspondences, so all strands to or from them will not be used, in other words they can be deliberately disconnected by the mapping. 

As many \glspl{mas} are distributed, it is impractical for the source and the target to both maintain a copy of the mapping.  The mapping will change regularly---particularly early in the learning process---and maintaining multiple consistent versions at sources and targets is needlessly complex.  The target never needs to know from which source state a particular transfer came.  In addition, \gls{ptl} is source driven, it follows that the mapping should be as well.  The source selects the data to transfer, so by having the mapping pre-translated information can be sent in transfers.
\subsection{On-Line Mapping}
When two agents are attempting to build an \gls{itm} without a common understanding of the environment in an on-line manner there is not enough time to fully calculate an effective \gls{itm} prior to it being used, so one must be built quickly and improved over time.  The na{\"i}ve approach is to produce an \gls{itm} that maps state-action pairs randomly\nobreak{\footnote{When using \glspl{itm}, it is reasonable to assume the agents are heterogeneous in some way and any representational or state naming similarly does not imply intrinsic similarly, hence matching based on state names is not used.}} from source to target.  This random mapping must then be adjusted.  There two methods to adjust the mapping.
\subsubsection{Vote-Based Mapping}
%done SECTION voting approch and how it can be crowd soursed through catogories of agent
From the starting point of a randomly populated \gls{itm} between a source and target.  The agents need to improve the \gls{itm} as it is almost certainly inaccurate.  One way of doing so is to have the target provide feedback to the source.  Upon receiving a transfer from a source, the target makes its merging decision using a merge method \gls{mm} (disused in Chapter~\ref{chapPTL}).  The target agent then produces a feedback tuple based on this decision.  The outcomes are binary; to merge or not to merge.  This is encoded as $\pm1$.  If the target agent chose not to merge a particular transfer, then it will send back negative feedback to the source.  Alternatively, it can reinforce that particular strand of the \gls{itm} by providing positive feedback.  When the source agent receives feedback, it accumulates it for the strand in question.  If the strand's value falls below a threshold (0 works well), then it is deemed not effective and remapped to some other state.  If there are states in the target without strands leading to them, then these are preferred.   For example, if the target agent receives a transfer to some state-action pair $Q_{target}(S,A)$ and it decides not to merge this transfer (i.e., the received information does not agree with local information), the target agent will send the feedback tuple $[Q_{target}(S,A), -1]$ indicating that particular strand of the mapping that lead to $Q_{target}(S,A)$ is inaccurate and should be changed.  Alternatively, if the transfer was merged this strand can be reinforced with the feedback tuple $[Q_{target}(S,A), 1]$.  If a perfect mapping\nobreak{\footnote{One in which each state-action pair finds a corresponding pair in the target where its knowledge is merged.}} exists, over many iterations the bad strands will be removed and only good ones will remain.  The more likely case is that only some of the source's state-action pairs will find correspondences in the target agent's state-space, in this case the sufficiently trained mapping will consist of good strands and fluctuating strands.  Where the fluctuating strands are constantly being rejected and trying new state-action pairs.  As their transfers will not be merged they have no impact on learning performance, this means they need not be removed which removes the complexity of checking if each strand has tried each of the target agent's state-action pairs.  Leaving the fluctuating strands prevents accidental removal of potentially useful transfer routes.  This could happen if the target learnt something that was not representative in a state (i.e., the value was not representative of the `true' value) and its correct strand was rejected based on this wrong information.  Allowing the fluctuating strands to retry states prevents this.

As it can take a considerable amount of time to find a correct mapping by voting, the mappings can be produced collaboratively by several agents.  In this case the sources must be mutually heterogeneous as must the targets.  Regardless of whether single or multiple agents are involved in the learning the mapping, it fits into the \gls{ptl} algorithm the same way (see Algorithm~\ref{algMapLearn}).  The starting section of the algorithm (marked by the right brace) is \gls{ptl} as described in Chapter~\ref{chapPTL}.  The extension to allow \glspl{itm} to be learnt is from line~\ref{algMapLearnMapping}.  \Call{FeedbackToSource}{} gets whether or not the received states were merged, it then sends the feedback tuples for these states to their respective sources.  If the Boolean variable $learningMapping$ is true, then \Call{ReceiveFeedback}{} gets any feedback tuples for the current agent to apply to its \gls{itm}.  These tuples are then iterated through and applied.  If the strand is determined to be not effective by its source's state's feedback score (accessed by \Call{getTotalFeedback}{$Q_{sent}(S,A)$}) and the $feedbackThreshold$, then \Call{RemapState}{$Q_{sent}(S,A)$} is used to replace it.
\begin{algorithm}[hbt!]
\begin{algorithmic}[1]
\State \textit{After \gls{rl} Update}
\ForAll{Agents $n\in N^{a}$} 
	\State $output\gets$ \Call{SelectKnowledge}{\gls{ts}, \gls{sm}}\Comment{Choose knowledge to send}\label{algMapLearnSelect}
	\State $output\gets\chi_{A_{a}\rightarrow n}(output)$\Comment{Translate knowledge}
	\State \Call{SendKnowledge}{$output$, $n$}
\EndFor \hspace*{17em}%
\rlap{\smash{$\left.\begin{array}{@{}c@{}}\\{}\\{}\\{}\\{}\\{}\\{}\\{}\\{}\end{array}\color{black}\right\}%
\color{black}\begin{tabular}{l}\gls{ptl} algorithm \\as in Chapter~\ref{chapPTL}.\end{tabular}$}}
\State $input\gets$\Call{ReceiveKnowledge}{}
\ForAll{$Q_{received}(S,A)\in input$}
	\State \Call{Merge}{$Q_{received}(S,A)$, \gls{mm}}\Comment{Incorporate new knowledge}\label{algMapLearnMerge}
\EndFor
\State \Call{FeedbackToSource}{}\Comment{Feedback in case neighbours are learning}\label{algMapLearnMapping}
	\If{$learningMapping$}\Comment{Learn mapping component}
		\State $feedback\gets$\Call{ReceiveFeedback}{}
		\ForAll {$[Q_{sent}(S,A),value]\in feedback$}
			\State\Call{ApplyFeedback}{$Q_{sent}(S,A), value$}\Comment{value either $-1$ or $+1$}
			\If{\Call{GetTotalFeedback}{$Q_{sent}(S,A)}<feedbackThreshold$}
				\State \Call{RemapState}{$Q_{sent}(S,A)$}\Comment{Was a bad strand}
				
			\EndIf
		\EndFor
	\EndIf
\end{algorithmic}
\caption{Learning an \acrlong{itm} by Votes}
\label{algMapLearn}
\end{algorithm}
\subsubsection{Ant-Based Mapping} %TODO amke about ants
%done SECTION other approches tried
%todo why not just sample the reward function to the value function not for a mapping
An alternate approach to mapping takes advantage of the fact that in most real-world systems the environment does not actually provide reward---as expected in the normal \gls{rl} pattern---but agents self-calculate it based on the state of the environment.  This distinction is significant as it means that the agent has access to a function that approximates its value function, its reward function.  The target task can share this with potential source tasks, that can use this to generate virtual experiences.  These virtual experiences are then provided to ants for an Ant-Colony Optimisation~\citep{dorigo1996ant}.  These ants try to find good correspondences between the virtual sample of the target agent's reward function and the source agents own.  If a good correspondence is found, it will likely be a good state to transfer to, as the value functions are likely similar as well.  Similar reward is a good indicator that the states will have similar values when the transition function and environmental variability are accounted for.  Conceptually this approach is related to Dyna~\citep{sutton2012dyna}, but does not require a model.  It too can be used to collaboratively produce a mapping, as long as the collaborative sources are heterogeneous as are the targets.  The number of ants used and what threshold are required for good correspondences, depend on the application.  Obviously the greater the number of ants, the better the chance of finding correspondences quickly.  The smaller the threshold of difference between reward, the more difficult correspondence are to find.

When used with \gls{ptl}, Ant-Based can happen in parallel to the normal \gls{rl}/\gls{ptl} execution (assuming parallel execution is supported, otherwise it can be interleaved with \gls{ptl} in any way).  A state in the target is chosen, its reward function sampled and the ants are started.  When the ants finish, the scores for the states they tried are compared to each other and  to the threshold.  A strand is selected based on the result.  The threshold is used to prevent ineffective strands being included in the mapping.  In the Algorithm~\ref{algMapAnt}, this process is shown.  In it the use of subscript $s$ or $t$ indicates which state-space has a state or action, source or target.  The process is not limited to only mapping unmapped states, it can remap states as well.  This is particularly important when there are few ants used, as it is unlikely to find the best strand when only testing a small subset of strands.
\begin{algorithm}[hbt!]
\begin{algorithmic}[1]
\State$targetReward\gets$\Call{GetTargetReward}{$(S_t, A_t)$}
\ForAll{Ants}
	\State$sourceReward\gets$\Call{GetSourceReward}{$(S_s, A_s) \in StateSpace_{source}$}
	\State$antScore\gets$\Call{AbsolouteValue}{$sourceReward-targetReward$}
\EndFor
\State\Call{ChooseBestAntAndMap}{$(S_t, A_t)$, $threshold$}
\end{algorithmic}
\caption{Learning an \acrlong{itm} by Ants}
\label{algMapAnt}
\end{algorithm}

%todo SECTION conclusions
%todo MENTION formalism of auto config componant and leanrned mapping componant
%todo MENTHION after experiments draw concluctions and redo add the thing from paper about rules for what is good when