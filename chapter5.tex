%\chapter{Self-Configuration}
%\label{chapAutoConfig}\pagestyle{chapterpage}
%\setlength{\epigraphwidth}{0.71\textwidth}
%\epigraph{We are all born ignorant, but one must work hard to remain stupid.}{Ben Franklin}\glsresetall\noindent
\section{Self-Configuration}\label{chapAutoConfig}
This chapter will introduce the Self-Configuration component, the purpose of which is to make \gls{ptl} more effective in changing environments.  This component is responsible for setting \gls{ptl}['s] parameters.  It does this by monitoring the environment and how the agent is performing, if either of these things deviate from expectations, then parameters are changed to remediate this.
\subsection{Introduction}
As discussed in Section~\ref{2sectEnviro}, there are three source of change that could affect the representativeness of a state's value: dynamicity, non-stationarity and other agents.  \gls{rl}---and by extension \gls{ptl}---learns a value for a state-action pair that includes the variability of the environment.  A state's value can be seen as having four constituent parts that are as follows with the latter three being the variability:
\begin{enumerate}[label=(\Alph*),ref=(\Alph*)]
\item \textbf{Inherent Part} is the component of value that the state would have gotten if it was in a simple environment.  It includes any natural variability in the reward received in the state. \label{5enumInherent}
\item \textbf{Neighbour Part} is the change in value due the the effects of other agents.
\item \textbf{Dynamic Part} is the effect of any dynamic change in the environment.
\item \textbf{Non-Stationary Part} is the share of the value that is due to changes in the environment that have happened since time zero. 
\end{enumerate}

Of these, the non-stationarity part is the most significant, as it can completely invalidate the other parts which increases the amount of learning to do and may reduce the effectiveness of \gls{ptl}.

Different methods of selecting data (\gls{sm}) and merging (\gls{mm}) work well in different environments.  Choosing the correct approach is important as \gls{ptl}['s] performance is affected by these methods.  The method used must be capable of changing, as in non-stationary environments, the best method to use may change over time.  Equally, natural progression though the learning process may change which is the best scheme to use; knowledge reuse will differ from early in the learning process to the end of it.  There are two ways this can be approached.  The first is the system can detect and categorise the environment that it is in and based on the resultant categorisation, select appropriate parameters.  The other method is to attach learning processes to \gls{ptl}['s] parameters and have them learnt by agents.  Either method opens a further possibility for transfer as agents can transfer effective sets of parameters for their environments.  Adding an extra level of learning will increase the time taken to learn by too much for any benefit to come from accelerating learning, so the predictive version will be used here.  The learning-based version will also require that the characteristics of an environment's change can be accurately represented at a high level, so that this meta-learning process can know its state.  It will also need several samples of each type of change to learn what is best for each one, this is impractical unless the environment is highly periodic or episodic.
\subsection{Environment Detection}
Detecting change in the environment is a complex issue and not the main topic of thesis, so a well known approach will be used.  Further work could be done to detect other types of change and to do so more accurately.  For example, predictive modelling~\cite{Marinescu2015} or statistical techniques~\cite{raza2015ewma} can be used.

The first step in reacting to changing environments is to monitor the environment.  Good monitoring of the environment allows divergence to be identified and reacted to.  Agents have limited perception of the environment, even in fully observable environments.  Generally, an agent will only know its own state, actions, reward and value function.  These are the only ways that an agent can perceive the environment without requiring extensive additional modelling, so one or more of them must be used to detect change in the environment.  Using state alone only tells the agent where in the environment it is, not what caused it to get there.  It also is not able to provide information about value changes.  The value function and reward can be used to detect change, but as a state's value is not equal to reward because of the update rule, it can be a poor indicator if there is natural variability in the reward.  Actions provide no information about the environment directly, but comparing results with previous instances can provide information.  This is problematic too if the actions themselves have variable results.  This leaves reward as the best metric to use to detect change.  The reward received in a state is a representation of how good that state is at the present moment.  A change in the environment that requires remedial action will affect reward before any of the other sources of information available to the agent.  So, if an agent maintains a history of the reward received at each state-action pair and checks for divergence, then state-action pairs can be categorised as either static or changing.  If sufficiently many states are categorised as changing, then it is known that the environment is changing.
%now catorgorisation 
\subsection{Environment Categorisation}
Once it is known that a state has changed, an agent needs to decide how many states need to be categorised as changing for the environment to be so categorised.  If only one state is found to be changing, then it is likely an error in the change detection.  It is unlikely that a significant environmental change would be solely confined to one state.  If an agent waits for all states in the environment to report change, then there is a significant delay in the agent's reaction to change.  Each state needs to be experienced to sample the reward and thereby determine if it is changing.  So, the threshold of changed states must be set to reduce the risk of reacting to errors, while still providing a timely response.  Obviously this will differ between environments, so it needs to be set by the agent.  In \gls{ptl}, the agent is tracking the number of times it visits states for both knowledge selection and merging, this count can be used to identify states that are frequently visited.  States that are frequently visited are particularly beneficial for both change detection and categorisation for several reasons.
\begin{enumerate*}[label=(\arabic*)]
\item Reward has been sampled more, so the model is better than in other states.
\item They are likely important states, so any change in them will have a greater impact on performance.
\item They are visited more regularly and will take less time to visit again, so change detection can happen faster.
\end{enumerate*}
What counts as `frequently visited' will depend on the environment, but any state representing over $10\%$ of visits works well.  In environments with very few states or very many, no states or only one may reach this mark.  This reintroduces the susceptibility to reacting to errors, in these cases the most visited states representing approximately $30\%$ of all visits can be used regardless of their absolute value of visit count or its relationship to other states.  The figure of $30\%$ gives an expectation that one of the states in this set will be visited every $3$ time steps, which give the potential for timely change detection.

Having categorised an environment as changing, the type of change needs to be identified.  There is no hard set of rules that can be used to categorise change, as it can vary both spatially and temporally.  Agents will have to estimate the type of change they are experiencing and react accordingly.  The following categories are useful and commonly found in environments:
\begin{itemize}
\item \textbf{Environment Based Change}
\begin{itemize}
\item \textbf{Slow Change} is change that has little impact on the reward received over short time periods, but continues until it impacts significantly.
\item \textbf{Fast Change} is significant over one time step and continues for several.
\item \textbf{Sharp Change} is significant change that happens within one time step.
\end{itemize}
\item \textbf{Other Sources of Change}
\begin{itemize}
\item \textbf{Neighbour Change} is when the impact of behaviour of the neighbours in aggregate changes from its previous level.
\item \textbf{Self Change} is when the agent is the source of change, it could be the addition of a new policy, more powerful actions etc.
\item \textbf{Finished Learning} can cause a change in what is required from \gls{ptl} as an agent no longer needs information to support learning.
\end{itemize}
\end{itemize}
Differentiating between the two sources of change is beyond the capabilities of reward-based detection and requires more sophisticated detection, however, those changes in the second category appear as being the first from a reward point of view.  Which means they can still be reacted to without the correct categorisation, the reaction may not be optimal but it can react.  Assigning change to one of these categorising depends on the detection method, but here it is based on the magnitude of the deviation from the modelled reward and the time period it occurs over.
\subsection{Change Mitigation}
Once the environment is found to be changing, the agent must decide how to react and what parameters to change, if any.  Regardless of how the environment is categorised, when an agent reacts to a particular category it can change one or more of the following:
\begin{itemize}
\item \textbf{Knowledge Selection Method (\gls{sm})}, how data is selected can be changed, if the environment is not changing the agent can be less cautious with its transfer, sharing more less-frequently-samples-states.
\item \textbf{States per Transfer (\gls{ts})} will vary based on the number of potential transfer targets.  More agents mean more states may have changed, so more information should be sent.  Transferring too many states can lead to sharing states that have not been recently sampled which risks sharing outdated information.
\item \textbf{Merging Period or Method (\gls{mm})} will change with the reliability of the received information and as an agent becomes more confident in its own state.
\item \textbf{Between Which Agents} will change if the particularly good pairs of agents are found.
\item \textbf{\gls{rl} parameters} can allow the agent to be more permissive with its learning and potentially learn to adapt to its new circumstance.  With more extreme change, a completely new learning phase can be triggered\nobreak{\footnote{Not considered here, as it is out of the scope of \gls{ptl}.}}.
\end{itemize}
In general, when the the environment is currently changing the agent should be more cautious.  This will prevent misleading information from being shared.  After change has finished more information should be shared, so the agent can relearn more quickly.  When the environment is not changing they should settle to their normal behaviour, whether this is designer encoded or configured based on how long the agent has been learning.
\subsection{CUSUM}
The approach to environmental change detection used here is \cusum{}.  \cusum{} is a statistical method that has been used in industrial quality control~\citep{hawkins1987self}.  It uses a model of the mean and standard deviation of a discrete time series process and parameters for false positives and negatives to detect when a process is out of control.  It operates using a cumulative sum, so it can detect small changes in mean.  It typically operates on processes with a known or targeted mean and standard deviation.

As in \gls{rl}, the mean and variation of the reward for a state can not be known a priori, the first samples are used to build a model of them.  This requires that the process's initial rewards are representative of those provided by the current environment.  This will almost always be the case.  If not, it will learn a model that is not representative (what \gls{rl} will also be not representative), then it will detect a change as the environment goes back to `normal'.  The model can then be retrained on the correct reward.  This means that regardless of if the model is built when change occurs, it can be detected.  This allows the model building to be done over several time steps.  The longer the model is built for, the better it can represent the inherent part~\ref{5enumInherent} of the environments variability in the environment, as it has more samples to model.

Retraining the model is needed after every change in the environment, as there is no guarantee that the environment will return to its previous reward distribution.  If the model is not retrained, it will be impossible to detect further change, as the wrong model will consistently report change.  For example, a model trained with the sequence of reward $\{A \mid 10, 15, 20\}$ will have a mean of $15$ and a standard deviation of $5$, if a change in the environment then give rewards of $\{B \mid 100, 150, 200\}$, this would be detected as a change.  If the model was not retrained and reward kept being drawn from $B$, each instance would be reported as a change.  

\cusum{} is basically an upper and lower bounds on where a discreet value drawn from a modelled distribution can fall.  It is recursive, so even small (within the standard deviation) changes can be detected over time.  The upper bound is specified by the Equation~\ref{5eqUpper} and the lower by Equation~\ref{5eqLower}.
\begin{equation}
\centering
\label{5eqUpper}
S_{hi}(t) = \max(0,S_{hi}(t-1)+x_t - \hat{\mu}- k)
\end{equation}
\begin{equation}
\centering
\label{5eqLower}
S_{lo}(t) = \max(0,S_{lo}(t-1)-x_t + \hat{\mu}- k)
\end{equation}
$x_t$ is the sample for the distribution at time $t$, $\hat{\mu}$ is the estimated mean and $k$ is a design parameter.  There is an additional parameter $h$ which is used to determine if $S_{hi}$ or $S_{lo}$ are too large.  If either is greater than $h$, then it has changed.  The procedure on which \cusum{} is based is called V-Mask~\citep{montgomery2007introduction}.  In it the parameters $h$ and $k$ are the slopes of the mask.  If a point is found to be outside the mask, it is out of control.  The selection of $h$ and $k$ can be rather opaque, so they have been related to three other parameters which are as follows:
\begin{itemize}
\item \boldsymbol{$\alpha$} is the allowable false positive rate.  It must be greater than $0$.  This $\alpha$ is distinct from \gls{rl}['s] \gls{alpha}.
\item \boldsymbol{$\beta$} is the false negative rate.
\item \boldsymbol{$\delta$} is the deviation required to be detected.  It is expressed as a multiple of the standard deviation.
\end{itemize}
To convert from the easier to work with parameters to $h$ and $k$, the Equations~\ref{5eqD},~\ref{5eqH} and \ref{5eqK} are used.
\begin{equation}
\centering
\label{5eqD}
d = \frac{2}{\delta^2}\ln\left(\frac{1-\beta}{\alpha}\right)
\end{equation}
\begin{equation}
\centering
\label{5eqH}
h = dk
\end{equation}
\begin{equation}
\centering
\label{5eqK}
k = \frac{\delta \sigma_x}{2}
\end{equation}
$\sigma_x$ is the model's standard deviation and in Self-Configuration's use of \cusum{}, is therefore an estimate and can be replaced with $\hat{\sigma_x}$.  The selection of the actual parameters used can be reasonably permissive and risk false positives, as there is the extra step of aggregating states reporting change before making a decision.  This provides some resilience to false positives.  If $\delta$ is set low enough, $\beta$ is not too high and $\hat{\sigma_x}$ is correctly modelled, then any samples that fall on the border of detection (where false negatives occur most frequently) and are not detected will likely not have been caused by very significant changes in reward.  This adds a factor of safety for missing changes that will significantly affect performance.  This can be seen in Figure~\ref{5imgErrorRisk}\nobreak{\footnote{Figure based on an example by \citeauthor{natrella2010nist} with slight modification~\citeyearpar{natrella2010nist}.}} at times $14\rightarrow16$ where the process is out-of-control and in-control in quick succession.  The figure shows the output of the upper bound in Equation~\ref{5eqUpper} for a series of values.  The lower bounds is omitted as this series does not deviate downward significantly.  There is no need to build the model as described above, as the true mean is known to be $325$, so no sample mean is needed as an estimate.  From time $11$ the process being monitored has begun to vary significantly, but not sufficiently to cross the detection threshold.  The value of the process being controlled is in Figure~\ref{5imgProcess}.  The cumulative effect of the deviations prior to time $16$ prevent the rather large drop in the process's output at that time from inducing a significant drop in the \cusum's output.  This `memory' feature is desirable, as the changes to the mean are what should be tracked rather than variations in individual samples.
%hk a b d and how these are not too imnportant
\begin{figure}[hbt!]
\centering
\includegraphics[width=\linewidth,keepaspectratio]{figures/chap5ErrorRisk}
\caption[\cusum{} Change Detection]{\cusum{} Change Detection Output from Process in Figure~\ref{5imgProcess}.}
\label{5imgErrorRisk}
\end{figure}
\begin{figure}[hbt!]
\centering
\includegraphics[width=\linewidth,keepaspectratio]{figures/chap5Process}
\caption[Samples from a Distribution (Mean = 325)]{Process used in Figure~\ref{5imgErrorRisk}.}
\label{5imgProcess}
\end{figure}
%menthin self start build model in my approch 

The magnitude of \cusum's output grows with the process's deviation from the mean.  This allows the type of change to be categorised.  A \cusum{} output of small magnitude is likely the result of slow or small change, while larger magnitudes indicate more significant change.  In this example, the current $S_{hi}$ rises at approximately one unit per time step in response to a final sample five standard deviations from the previous mean (controlled standard deviation calculated over the first twelve samples at $5.1$).  This is a relatively small change and could be categorised as such.  There are no single values for $S_{hi}$ or $S_{lo}$ that can be used as a cut off between change types, as they are only loosely defined and application dependant, but approximately $10h$ and $20h$ work well as points to cut off from slow to fast and fast to sharp respectively.  

Change in the reward is unlikely to be proportional everywhere in the state-space, reward in some states may increase while others may fall.  This means some states may report change through $S_{hi}$, while others with $S_{lo}$.  Both these two numbers need to be used to categorised change.  As each state can only have one at a time---reward in a particular state that has changed can only be higher or lower than previously---and both are positive values that grow with increased deviation, which they can be combined with an average of their magnitudes.  This can be done irrespective of the direction of the change, as remedial action is based on the significance of the change, rather than the new value that will be learnt.  The number of states exhibiting change is also an important heuristic for error detection.  If only one state-action pair has changed and no others appear to have changed when sampled, it is likely the state-action pair with the changing value was incorrectly modelled.  While if several values change, action may need to be taken.  Errors can be corrected by remodelling the reward.

The Algorithm~\ref{algConfig} from Line~\hyperref[algConfig2]{\ref{algConfigChange}} %todo check this ref if algoritm combined
is the Auto-Configuration component, the previous lines are as presented in Chapter~\ref{chapMapping}.  \Call{UpdateCUSUM}{$Q_{\gls{rl}}(S,A)$, $reward$} adds another reward to the monitoring process for the state-action pair $(S,A)$.  The \cusum{} is then calculated for this to see if there has been change.  The number of changed states and the average magnitude of change are then used to determine if a remedial action should be taken.  %The potential changes are discussed in Section~\ref{5sectRemedial}.  
A pictorial representation of the algorithm is in Figure~\ref{5imgActivityPTLHigh}. 
%todo auto detect environment general subsection cusum .cant use self starting because of why  and catorgorisation
%todo set things based on rules or huristics like if stable lots of states
%todo CODE reward and change in environment must reflect it add this in to where i talk baout reward being self calcing 
%todo CODE add in single state re open if changes a lot
%todo CODE try transfering not states with bad cusums
%todo CODE percentage threashhold will depend on the number of states regularly visited under normal operation.  if 100 states only 10 regular thold<10
%todo CODE might need record of las n changes as a change might not be evperienced at a state for a time
%todo CODE change model build based on 
\begin{algorithm}[hbt!]
\begin{algorithmic}[1]
\State \textit{After \gls{rl} Update}
\ForAll{Agents $n\in N^{a}$} 
	\State $output\gets$ \Call{SelectKnowledge}{\gls{ts}, \gls{sm}}\Comment{Choose knowledge to send}\label{algConfigSelect}
	\State $output\gets\chi_{A_{a}\rightarrow n}(output)$\Comment{Translate knowledge}
	\State \Call{SendKnowledge}{$output$, $n$}
\EndFor \hspace*{17em}%
\rlap{\smash{$\left.\begin{array}{@{}c@{}}\\{}\\{}\\{}\\{}\\{}\\{}\\{}\\{}\end{array}\color{black}\right\}%
\color{black}\begin{tabular}{l}\gls{ptl} algorithm \\as in Chapter~\ref{chapPTL}.\end{tabular}$}}
\State $input\gets$\Call{ReceiveKnowledge}{}
\ForAll{$Q_{received}(S,A)\in input$}
	\State \Call{Merge}{$Q_{received}(S,A)$, \gls{mm}}\Comment{Incorporate new knowledge}\label{algConfigMerge}
\EndFor
\State \Call{FeedbackToSource}{}\Comment{Feedback in case neighbours are learning}\label{algConfigMapping}
	\If{$learningMapping$}\Comment{Learn mapping component}
		\State $feedback\gets$\Call{ReceiveFeedback}{}
		\ForAll {$[Q_{sent}(S,A),value]\in feedback$}
			\State\Call{ApplyFeedback}{$Q_{sent}(S,A), value$}\Comment{value either $-1$ or $+1$}
			\If{\Call{GetTotalFeedback}{$Q_{sent}(S,A)}<feedbackThreshold$}
				\State \Call{RemapState}{$Q_{sent}(S,A)$}\Comment{Was a bad strand}
				
			\EndIf
		\EndFor
	\EndIf
% to break pages move from here
\algstore{config} %in part 1
\end{algorithmic}
\caption[\acrlong{ptl}, Mapping and Self-Configuration]{\acrlong{ptl}, Learning Mapping and Self-Configuration}
\label{algConfig}
\end{algorithm}
\note[adam]{This algorithm is split in two as it tends to fall on two pages when text is changed.  Can make it one in final version but breaking it nicely take a bit of effort}
\begin{algorithm} [hbt!]%part 2
\ContinuedFloat
\begin{algorithmic}
\algrestore{config}
\If{$detectingChange$}\label{algConfigChange}\Comment{Change detection component}
	\State \Call{UpdateCUSUM}{$Q_{\gls{rl}}(S,A)$, $reward$}
	\State $numStates \gets$\Call{GetNumberOfChangedStates}{}1
	\State $changeMag \gets$\Call{GetAverageMagnitudeOfChange}{}
	\If{$numStates\ge$\Call{GetNumberOfRegularlyVisitedStates}{}}
		\If{$changeMag\le fastThreshold$}
				\State \Call{ReactToSlowChange}{}
			\ElsIf{{$changeMag\le sharpThreshold$}}
				\State \Call{ReactToFastChange}{}
			\Else
				\State \Call{ReactToSharpChange}{}
		\EndIf
	\EndIf
\EndIf
\end{algorithmic}
\caption*{\gls{ptl}, Learning Mapping and Auto-Configuration (continued)}
\label{algConfig2}
\end{algorithm}
\begin{figure}[hbt!]
\thisfloatpagestyle{chapterpagenonum}
\centering
\includegraphics[height=\textheight,keepaspectratio]{figures/chap5ActivityPTLHigh}
\caption[Activity Diagram of \acrlong{rl}, \acrlong{ptl} and Self-Configuration]{Activity Diagram of \gls{rl}, \gls{ptl} and Self-Configuration.}
\label{5imgActivityPTLHigh}
\end{figure}

%write this based on results
While the most effective \gls{ptl} configuration for a particular type of change is evaluated in Chapter~\ref{chapEvaluation}, some basic heuristics can be applied.  When an agent's environment is not changing and it is early in its learning process, it should transfer states that it has visited the most, as these are more reliably sampled.  Later in the learning process, states with the most impact on performance should be shared, for example, those with the highest value.  When the environment is changing slowly, newer or received information will be best, as it likely has not been invalidated by change.  During fast change transferring no information or only the most recently sampled may be best, as all previously learnt information may have been invalidated.
\begin{figure}[hbt!]
\centering
\includegraphics[width=\textwidth,keepaspectratio]{figures/chap5ActivityPTLSelection}
\caption[Activity Diagram Self-Configuration]{Activity Diagram Self-Configuration.}
\label{5imgActivityPTLSelection}
\end{figure}

\section{From other places}
\subsection{PTL}
\subsection{Design Influences}
%done SECTION conclutions
%todo Content text version of this tabel
\gls{ptl} is designed to accelerate learning in \glspl{mas}.  In Chapter~\ref{chapRelated} characteristics required for accelerating learning and problems that affect learning rate were identified.  The Table~\ref{3tabTraits} shows how \gls{ptl} addresses them.
\begin{table}[hbt!]
\centering
%\resizebox{\textwidth}{!}{
    \begin{tabulary}{\textwidth}{lcL}    
    \toprule
    \multicolumn{1}{c}{\textbf{Problem}} & \textbf{Description} & \textbf{Addressed by}    \\
    \midrule
    Scalability & \ref{3enumScale} & Operates between a configurable number of agent pairs at any one time.  \\
    Complex Environments & \ref{3enumComplex} & Knowledge constantly shared, so change can be reacted to.  \\
    No Designer Input & \ref{3enumDesign} & Capable of self-configuration (see Chapter~\ref{chapAutoConfig}).  \\
    On-line & \ref{3enumOnline} & No knowledge is needed prior to target execution.  \\
    Exploit Relatedness & \ref{3enumRelated} & Agents are capable of reusing knowledge.  \\
    \cmidrule(l{1em}r{1em}){1-3}
    Sample Variation & \ref{3enumSample} & Additional knowledge reduces samples needed at variable states. \\
    Sparsely Visited States & \ref{3enumSparse} & Neighbours help by providing extra information.\\
    Credit Assignment & \ref{3enumCredit} & States are moved to their correct value without backpropagation. \\
    \bottomrule    
    \end{tabulary}
%    } %todo get version from charpt 7
    \caption[Characteristics of \acrlong{ptl}]{Characteristics of \gls{ptl}.}
    \label{3tabTraits}
\end{table}
Scalability in particular guided the design of \gls{ptl}.  Rather than having the target request knowledge about a particular state---which would significantly increase required communication and processing demands---the source chooses what to share.  By allowing \gls{tl} to happen on-line, \gls{ptl} can leverage the relatedness of agents in a \gls{mas} to accelerate learning. 
%done SECTION design principals source driven minimal communication may not be full section %why online
