\chapter{Implementation}
\label{chapImplementation}\pagestyle{chapterpage}
\setlength{\epigraphwidth}{0.88\textwidth}
\epigraph{You have to have an idea of what you are going to do, but it should be a vague idea.}{Pablo Picasso}\glsresetall\noindent
This chapter describes the \gls{rl} library used and its extension for \gls{ptl}.  It will also introduce the simulators used in Chapter~\ref{chapEvaluation} and their integration with the library.  This chapter contains no new information about \gls{ptl} and as such is not crucial to understanding it.  However, it would be needed for the replication of results, and the set-up is important for the evaluation in Chapter~\ref{chapEvaluation}.  The code is available on GitHub\nobreak{\footnote{GitHub is a register trademark (see \url{github.com}).}} with further instructions on how to run it at \url{github.com/tayloral/GridLAB-D_RL}.
%DONE when reading check for introduceing i wrote lib twice
\section{Library Design}
%what we were trying to do
Due to the way most \gls{rl} libraries stored data and handled time, they were unsuitable to build \gls{ptl} on, so one was written from scratch.  The library is written in \cpp{}, for easy integration with one of the main simulators, \gridlab~\citep{chassin2008gridlab}.  Consideration was also given to deploying agents on real-world embedded devices.  Many can run \cpp{} which is not true of many other languages.  Aside from this, it is well-known, commonly used and reasonably platform independent.  

The main design goals were that agents could be easily created for new applications and integrated into them.  The structure was kept general enough, so that other algorithms could be integrated into the same structure as long as they operate in discrete time.  To achieve this, a top down approach was taken based on the requirements of the \gls{dwl} algorithm.  \gls{dwl} was used as an exemplar algorithm as it requires support for multiple agents, objectives and environment, each of which needs to be implemented separately.  This gives a structure into which other \gls{rl} algorithms could be implemented.  \gls{dwl} requires that the following steps happen:
\begin{itemize}
\item \textbf{Select Action} chooses what action will be executed.
\item \textbf{Execute Action} applies the selected action to the environment.
\item \textbf{Update Local Objectives} informs an agent about how the environment changed.
\item \textbf{Pass Messages} informs neighbours of what has happened.
\item \textbf{Update Remote Objectives} updates an agent based on what the neighbours have said happened to them.
\item \textbf{Transfer Information to Others} selects and sends \gls{ptl} messages to neighbours.
\item \textbf{Translate Knowledge} uses an \gls{itm} to translate knowledge to the recipient's representation.
\item \textbf{Receive Information from Others} receives and merges \gls{ptl} messages from neighbours.
\item \textbf{Update Mapping} happens if the agent wishes to learn an \gls{itm}.
\item \textbf{Self-Configure} adjusts the parameters of \gls{ptl}.
\end{itemize} 
The communications are all asynchronous, but tighter requirements can be added.  There is no requirement that these steps all happen or in any set order\nobreak{\footnote{Obviously to learn anything reasonable, an agent should learn from the effect of its own action which implies a causal ordering, but this is not enforced by the library.}}.  These steps allow most discrete time algorithms to operate, so are a good basis around which to build the library.  It also facilitates the integration of simulators as an external process to the agent.  The execute action step is simply replaced by a time-step of the simulator.  Similarly, the pass messages step is decoupled from actual communications and simply serialises messages for some other process to deliver.  All of these specialisations are handled by the actual use of the library and allow the library's core functionality to be agnostic of a particular deployment.  The library was built from scratch as existing \gls{rl} libraries were not flexible enough to use with the external simulators without considerable effort.  Use of external libraries can also limit deployment on some embedded platforms.

\begin{figure}[hbt!]
\centering
\includegraphics[width=\linewidth,keepaspectratio]{figures/chap6class}
\caption[Class Diagram of the \acrlong{ptl} Library]{Class Diagram of the \acrlong{ptl} Library.}
\label{6imgClass}
\end{figure}
\section{Library Structure}%flow diags etc
The library is organised, so an agent (DWLAgent in Figure~\ref{6imgClass}) contains everything needed to perform \gls{rl}.  The location of the main methods for each component are identified by colour.  Red for \gls{ptl} component, green for Learnt Mapping component and blue for Self-Configuration component.  A Policy is a container for a single objective, if an agent is learning to meet this objective with any Q-Learning-based algorithm it will need a WLearningProcess.  If the agent only has one objective, the fact that W-Learning is used does not matter, as there is no inter-objective arbitration that can take place; there is only one action to choose from at any time.  In the case of multiple objectives (in \gls{dwl} for example), policies can be either local or remote.  The local policy represents and agents own objective, while remote ones represent neighbours' objectives.  These three classes are the main parts of the required structure.  The environment drives what an agent must do at any one time.  

When an agent is required to select an action by the environment, it polls all its policies via the nominate method in DWLAgent.  Nominate asks each policy for an action suggestion.  Action suggestions are the action that an objective would like to be executed at the given time.  In \gls{dwl}, each action suggestion comes with a W-Value (weight based on its importance).  These W-Values are maintained by the W-Table.  The actual action contained in a suggestion is chosen by a class that extends ActionSelection.  ActionSelection gets the possible actions for the current state from the QTable (which also maintains the Q-Values).  Once nominate has the action suggestions from all policies, the one with the highest W-Value is provided to the environment.  The TaylorSeriesSelection class provides a directed but somewhat random search based on temperature much like Boltzmann does, but without the problem of exponentials exceeding bitwidth.  The temperature is on the range $[0, 1000]$, where $0$ is purely exploitation and $1000$ is random selection.  Anything in between these points uses the temperature as a probability of using random selection.  For example, a temperature of $400$, would choose a greedy action $60\%$ of the time and a random exploratory action the remainder of the time.  This is a novel method to avoid the bitwidth limitations of Boltzmann.

Once the environment has executed an action, DWLAgent's updateLocal method is called.  This provides each of the local policies with the environment's new current state.  Each local policy calls its own update function and performs Q-Learning and W-Learning updates.  The reward received can be passed in from the environment or some other external process.  If the environment lack the ability to calculate reward, then the WLearningProcess can self-calculate its own reward.

\gls{dwl} requires update messages be sent and received prior to calling updateRemote.  updateRemote then processes these messages and learns how the agents' actions affect its neighbours.  Transfers from \gls{ptl} also use the message passing interface.  It selects data to transfer with TransferToAll and readTransferedInfoIn merges it.  Both of these methods are affected by the Self-Configuration component.  Self-Configuration is encapsulated in DWLAgent's adaptAndReconfigure method and uses the \cusum{} value that each state-action pair has and calculates with its attached Cusum class.  Importantly, a function called finishRun in DWLAgent must be called to end each time-step.  It does several things including removing old action suggestions and messages.  Most of the data structures used are \cpp{'s} Standard Template Library containers, so minimal effort is required for others to use the library~\citep{Plauger2000}.  As the Standard Template Library is used, memory must be managed by the agent, this is done by finishRun releasing old messages once they have been processed.  This sequence of interactions between is shown in Figure~\ref{6imgSequencePolicy}. %DONE say more

While the current implementation of \gls{ptl} is built on top of \gls{dwl}, it would be relatively easy to adapt to other \gls{rl} algorithms.  The agent structure allows different temporal difference algorithms to be added with only a small change to the update rule.  Adding support for functional approximation or continuous state-spaces would require changes in the underlying \gls{rl} representation, but the transfer infrastructure would need only minimal changes to accommodate the new representation.  The most work would be required if support for policy-search were required, as both the representation and update rule would need to be changed.  The information exchanged may also need to change, as policy-search algorithms do not necessary have state-action values (which the code currently shares).  If policy values have to be passed rather than state-action values, then a small change would have to be made to the transfer infrastructure so it could interpret policy values.  Other than this there are few changes needed to the code to make it more generalisable. 
\clearpage
\begin{figure}[hbt!]
\thisfloatpagestyle{chapterpagenonum}
\centering
\includegraphics[angle=90,origin=c,width=\linewidth,keepaspectratio]{figures/chap6SequencePolicy} %,height=\dimexpr \textheight - 6\baselineskip\relax
\caption[Sequence Diagram of the \acrlong{ptl} Library]{Sequence Diagram of the \acrlong{ptl} Library.}
\label{6imgSequencePolicy}
\end{figure}
\FloatBarrier
\section{Library Use}
%extention of agent, make reward tie to simulator
To implement a \gls{dwl} agent using the library the first three things are needed, the fourth is needed to implement \gls{ptl}:
\begin{itemize}
\item \textbf{Extend DWLAgent} to produce a specialised agent for a particular application.  This will need to add policies with sets of states and actions.
\item \textbf{Extend Reward} if the environment is not capable of providing it.  Each policy needs a separate reward function, assuming that different policies encode different objectives.
\item \textbf{Provide Interpretation} for the agent.  This will need to turn the agent's action representation into something understood by the environment, the environment's state will need to be translated to the agent's representation.  Communications infrastructure should be provided as well.
\item \textbf{Transfer Set-Up}, if the agent is not learning an \gls{itm}, then one will need to be provided for \gls{ptl} to occur.  Pairs of agents for transfer also need to be specified, as there is no way to identify good transfer pairs otherwise. Pair selection can be done by the library, but not in a particularly intelligent way.
\end{itemize}
While the library can be further configured: parameters set, neighbours selected or other components added; the above is all that is required.
%\FloatBarrier
\section{PTL Component}
As was mentioned in Chapter~\ref{chapPTL}, at a high level, \gls{ptl} is two loosely coupled pairs of methods.  The first is the selection method \gls{sm} and the merge method \gls{mm}, the second is the communications wrappers for these.  When an agent wishes to transfer to its neighbours, it calls the transferToAllFromAll method.  The flow of gathering information is shown in Figure~\ref{6imgSequencePTL}.  As two components are involved in the transmission of knowledge, the colour scheme is kept the same as Figure~\ref{6imgSequencePolicy}.  Green indicates the Mapping component; red, the \gls{ptl} component and black are generic methods.  This polls all of its objectives for the knowledge they want to transfer.  This is done through the transferToAllFromOne method.  This method actually applies the selection method \gls{sm} to the value function for the objective.  Applying the selection method iterates through the value function stored in the QTable and gathers \gls{ts} state-action pairs.  These pairs are then passed to the Mapping component to be translated.  The translated version of the knowledge is then passed to the communications.  The communication is done by serialising the knowledge to transfer into an XML-like format before transmission.  The XML-like format is used so that knowledge can human readable for debugging purposes.  The library supports sockets, shared memory and direct method calls to provided communication, but only at a low level, there is no routing or discovery or other high level services.  This allows the library to be used across networks, in multi-threaded applications and in a single process.
\begin{figure}[hbt!]
\thisfloatpagestyle{chapterpagenonum}
\centering
\includegraphics[width=\linewidth]{figures/chap6SequencePTL} %,height=\dimexpr \textheight - 6\baselineskip\relax
\caption[Sequence Diagram of \acrlong{ptl} Knowledge Selection]{Sequence Diagram of \acrlong{ptl} Knowledge Selection.}
\label{6imgSequencePTL}
\end{figure}
%\newpage%SPACING
The sequence of actions involved in the merging of received knowledge is shown in Figure~\ref{6imgSequencePTLmerge}.  Again, red indicates the \gls{ptl} component and black are generic methods.  When a transfer is received by the communications interface it sits in a buffer until \gls{ptl} is ready to process it.  Once it is ready, it parses the message and splits it into sections for different policies.  These sections are then passed to their respective policies.  The merge method \gls{mm} now coordinates the actual integration into the state-space stored in the QTable.  It gets the current value for the state-action pair and the metadata about the state.  It then uses this information to determine if it should be merged and how.  First, the heuristic that estimates if the transfer agrees with the expected converged value is used (discussed in Chapter~\ref{chapPTL}).  If it passes this test, then the new value is calculated by weighting the local knowledge and received knowledge based on the amount of times the state has been visited.  Finally the result of merging is stored.

\begin{figure}[hbt!]
\thisfloatpagestyle{chapterpagenonum}
\centering
\includegraphics[width=\linewidth]{figures/chap6SequencePTLMerge} %,height=\dimexpr \textheight - 6\baselineskip\relax
\caption[Sequence Diagram of \acrlong{ptl} Knowledge Merging]{Sequence Diagram of \acrlong{ptl} Knowledge Merging.}
\label{6imgSequencePTLmerge}
\end{figure}

\section{Learnt Mapping Component}
The two different methods of mapping operate in similar sequences; Vote-Based is shown in Figure~\ref{6imgSequencePTLmap}.  All of the methods used in it are part of the Learnt Mapping component, so no colouring is applied.  Once the \gls{ptl} component gives the Learnt Mapping component some knowledge to translate and an agent's objective to map it to, it finds the correct mapping then it begins searching the set of strands for the state-action pair that is to be mapped to see if it can translate the knowledge.  If it fails to find a strand, it checks to see if that strand has been mapped before, and if not, it maps it randomly and translates the knowledge.  If the state is not in the mapping and has been tried before it is assumed to be unmappable and is not mapped.  Once this is done for all state-actions to be sent, the result is returned.  The recipients of this knowledge will send back feedback when they apply their merge method \gls{mm} to the mapped knowledge.  This feedback is then sent to the source agent, and upon receiving this, the second half of the sequence diagram happens.  The feedback is applied and stored by the TransferMapping class.  The entire \gls{itm} is then scanned for any strands whose feedback score falls below the threshold for good mapping (usually zero, which means that any bad feedback causes the state to be remapped).  Any strands that are identified as requiring remapping are removed and their state-action pairs added to the unallocated pools (unless they were in other strands as well).  The unallocated pools are sets of state-action pairs which are not connected by strands.  When an \gls{itm} needs a new strand, it first looks in the unallocated pools for potential strands.  The rationale for this is that any state-actions that are mapped effectively will be left as they are while the rest of the mapping is developed.

Instead of the feedback process used in the Vote-Based mapping, the Ant-Based mapping has an update step using ants.  The ants use the same structure as Vote-Based mapping to store the pheromone, the closer relate two state-action pairs are, the more pheromone `votes' that strand receives.  The Ant-Based mapping is then checked the same way as the Vote-Based, any strands below a threshold are removed and added to unallocated pools.
\begin{figure}[hbt!]
\thisfloatpagestyle{chapterpagenonum}
\centering
\includegraphics[width=.7\linewidth]{figures/chap6SequencePTLVoteMap} %,height=\dimexpr \textheight - 6\baselineskip\relax
\caption[Sequence Diagram of Vote-Based Mapping]{Sequence Diagram of Vote-Based Mapping.}
\label{6imgSequencePTLmap}
\end{figure}
%\FloatBarrier
\section{Self-Configuration Component}
The implementation of the Self-Configuration component is less discrete than the other components.  Its main action is shown in Figure~\ref{6imgSequenceSelfConfig}.  The self-configuration process does not have the same temporal requirements as the rest of \gls{ptl}.  \gls{rl} needs some temporal ordering as the update must follow the action to learn correctly.  With \gls{ptl}, there is an expectation that knowledge will be shared and merged regularly so that it can improve performance.  Self-configuration is likely to happen much less frequently, as change in the environment is rare compared to the frequency of \gls{rl} or \gls{ptl}.  When there is no change in the environment \gls{ptl}['s] configuration is kept static, so the Self-Configuration component only has to do periodic monitoring until it detects change.  The regular updates to the \cusum{} are done following an \gls{rl} update.  As mentioned previously, each state-action pair has its own \cusum{} process that monitors if it is changing or not.  The latest sample of reward is added to \cusum{'s} model by default following an update.  The compareToThreshold method takes the result of polling all of the \cusum{s} and determines if change is occurring based on the number of changed states.  If sufficiently many states are changing, then it will be categorised as fast, slow or sharp (see Chapter~\ref{chapPTL}).  If fast change is happening, then the greatest change method is used with a small transfer size \gls{ts}.  This means only new information is shared, this information could have come from local information or from a transfer.  If slow change is detected, the most visited and converged states are transferred as these have a greater degree of confidence than the other potentially changed states.  If sharp change happens, only the most recently visited state or received states are transferred as all information is assumed to be potentially invalid.  If any type of change is detected, the merge method \gls{mm} has its visit counts reset.  This effectively drops the confidence in each state and the agent becomes willing to merge information that does not agree with its local knowledge.
\begin{figure}[hbt!]
\thisfloatpagestyle{chapterpagenonum}
\centering
\includegraphics[width=.7\linewidth]{figures/chap6SequencePTLSelfConfig} %,height=\dimexpr \textheight - 6\baselineskip\relax
\caption[Sequence Diagram of the Self-Configuration Component]{Sequence Diagram of the Self-Configuration Component.}
\label{6imgSequenceSelfConfig}
\end{figure}
\section{Simulators}
The simulators used will be described here from an implementation point of view.  Their selection and particular scenarios used will be presented in Chapter~\ref{chapEvaluation}.
\subsection{Mountain Car}
The Mountain Car is well-known in \gls{rl}~\citep{knox2011reinforcement}.  In it an agent must learn to drive a car up one side of a valley.  Its engine is insufficient to do this, so it must first reverse up the opposite side to gain enough potential energy to complete the task.  The parameters used are based on those used in \citep{sutton1998reinforcement}.  They are detailed in Tables~\ref{6tabMTCarParam} and~\ref{6tabMTCarUpdate}.  The reward was changed to this form, as the more common form, $-1 + height$, directly encodes the knowledge that height is good which is one less thing for the agent to learn.  It also creates two local maxima (one at each side of the valley), which makes the agent's goal somewhat ambiguous.  There is also a change to the scaling factor on the velocity update, so more ground is covered by one time-step.  This makes learning more difficult, as it covers more ground in one time-step which means each action has a greater effect on the environment.  The Mountain Car is a Markovian environment as the effects of actions at previous times are encapsulated in its current state variables.  This means that anything that happened at $T-n \forall n>0$ is completely included in the current state and there are no lingering effects.
\begin{table}[hbt!]
%DONE check using objective vs policy
\centering
%\resizebox{\textwidth}{!}{
\begin{tabulary}{\textwidth}{lcL}    
    \toprule
    \textbf{Parameter} & \textbf{Value} & \textbf{Description} \\
    \midrule
    %\multicolumn{3}{c}{\textbf{State Variables}}    \\
    %\cmidrule(l{3em}r{3em}){1-3}
    Position & $[-1.2, 0.6]$ & Quantised in to $4$ states variables with equal ranges with one additional goal state.\\
    Goal position & $0.6$ & \ditto\\
    Velocity & $[-0.07, 0.07]$ & Range split into $4$ states variables covering $\frac{1}{3}, \frac{1}{6}, \frac{1}{6}, \frac{1}{3}$ of the range respectively.\\
    \bottomrule
\end{tabulary}
    \caption[Mountain Car State Variables]{Mountain Car State Variables.}
    \label{6tabMTCarParam}
\end{table}
\begin{table}[hbt!]
\centering
%\resizebox{\textwidth}{!}{
    \begin{tabulary}{\textwidth}{lL}    
    \toprule
    \textbf{Parameter} & \textbf{Value} \\
    \midrule
    Actions & Left $-1$, Neutral $0$ or Right $1$\\
    Reward & $10$ if at the goal, $-1$ otherwise.  \\
    Velocity Update & $Velocity+action*-0.00375*\cos(3*Position)$  \\
    Position Update & $Position+Velocity$\\
    \bottomrule
    \end{tabulary}
    \caption[Mountain Car Update Rule and \acrlong{rl} Settings]{Mountain Car Update Rule and \acrlong{rl} Settings.}
    \label{6tabMTCarUpdate}
\end{table}

%\FloatBarrier
\subsection{Cart Pole}
The Cart Pole or Inverted Pendulum is similarly well-known~\citep{sutton1998reinforcement}.  In it, a cart must move horizontally so as to balance a straight inflexible rod that is attached to it but free to rotate.  The parameters used are in Table~\ref{6tabCartPoleParam}.  The pole was lengthened slightly to make the problem more challenging.  Its state variables and reward are described in Table~\ref{6tabCartPoleRL}.  The Equations~\ref{6eql} -~\ref{6eqthetadot} are used to update the state of the system.  The physical interpretation of the parameters is in Figure~\ref{6imgCartPole}.  Additionally $\tau$ is the length of a time-step, $\dot{x}$ is acceleration of the cart, $\dot{\theta}$ is the pole's acceleration and $m_c$ is the cart's mass.  The agent's goal is to keep the pole balanced for as long as possible.  The Cart Pole is Markovian for similar reasons to the Mountain Car, there are no effects from previous actions.
\begin{equation}
\centering
\label{6eql}
L = \frac{l*m}{2}
\end{equation}
\begin{equation}
\centering
\label{6eqx}
x = \tau*\dot{x}
\end{equation}
\begin{equation}
\centering
\label{6eqxdot}
\dot{x} = \tau*\frac{forceComponent - L* \dot{\theta}*\cos(\theta)}{m+m_c}
\end{equation}
\begin{equation}
\centering
\label{6eqforce}
forceComponent = \frac{f*action*L*\dot{\theta}^2*\sin(\theta)}{m+m_c}
\end{equation}
\begin{equation}
\centering
\label{6eqtheta}
\theta = \tau * \dot{\theta}
\end{equation}
\begin{equation}
\centering
\label{6eqthetadot}
\dot{\theta} = \tau * \frac{(g*\sin{\theta}-\cos(\theta)*forceComponent)}{\frac{\frac{4L}{3}-m*\cos(\theta)^2}{m+m_c}}
\end{equation}
\begin{figure}[hbt!]
\thisfloatpagestyle{chapterpagenonum}
\centering
\includegraphics[width=.65\linewidth]{figures/chap6CartPole} %,height=\dimexpr \textheight - 6\baselineskip\relax
\caption[Cart Pole Parameters]{Cart Pole Parameters.}
\label{6imgCartPole}
\end{figure}
\begin{table}[hbt!]
\centering
%\resizebox{\textwidth}{!}{
    \begin{tabulary}{\textwidth}{lC}    
    \toprule
    \textbf{Parameter} & \textbf{Value} \\
    \midrule
    Actions & Left $-1$ or Right $1$  \\
    Reward & $-20$ if the pole falls, $0$ otherwise.  \\
    Gravity & $9.8\ ms^{-2}$ \\
    Mass of Cart & $1\ kg$ \\
    Mass of Pole & $0.1\ kg$ \\
    Length of Pole & $1.4\ m$ \\
    Force Applied & $\pm10\ N$ \\    
    Simulation Time-Step & $0.01\ s$\\
    \bottomrule
    \end{tabulary}
    \caption[Cart Pole Parameters and \acrlong{rl} Settings]{Cart Pole Parameters and \acrlong{rl} Settings.}
    \label{6tabCartPoleParam}
\end{table}
\begin{table}[hbt!]
\centering
%\resizebox{\textwidth}{!}{
    \begin{tabulary}{\textwidth}{lcL}    
    \toprule
    \textbf{Parameter} & \textbf{Value}  & \textbf{Description} \\
    \midrule
    Cart Position & $[-2.4, 2.4]$ & Quantised in to $3$ states variables with equal ranges.\\
    Cart Velocity & $[-1, 1]$& Quantised in to $4$ states variables with equal ranges. \\
    Pole Position & $[-90^\circ, -6^\circ, -1^\circ, 0^\circ, 1^\circ, 6^\circ, 90^\circ]$ & Quantised in to $6$ states variables as indicated.\\
    Pole Velocity & $[-\infty^\circ, -50^\circ, 0^\circ, 50^\circ, \infty^\circ]$& Quantised in to $4$ states variables as indicated. \\
    \bottomrule
    \end{tabulary}
    \caption[Cart Pole State Variables]{Cart Pole State Variables.}
    \label{6tabCartPoleRL}
\end{table}
%\FloatBarrier
\subsection{GridLAB-D}
\gridlab{} is an electrical grid simulator produced by the U.S. Department of Energy ar Pacific Northwest National Laboratory~\citep{chassin2008gridlab}.  It simulates the grid with sub-second accuracy of the behaviour of electricity.  It is a finite difference based simulator, which means that elements of the simulation can be synchronised at different rates depending on how much they change.  It is relativity easy to change the structure of the grid and data logging is inbuilt.  Most importantly, it is open source, so it can be integrated with the \gls{ptl} library.

\gridlab{'s} integration with the library is more involved than integration with the previous applications.  The other simulators and the \gls{ptl} library could interact directly, as they were the only components involved.  The simulation could progress without requiring any temporal synchronisation, as either \gls{ptl} was happening or the simulation and the other was waiting.  This effectively kept their method calls synchronised, so \gls{rl} and \gls{ptl} were occurring at the right times to learn from the results of their actions.  In \gridlab{} there are other components that need to be synchronised to accurately simulate electricity flow, most notably Powerflow and Core (see Figure~\ref{6imgGridlab}).  Figure~\ref{6imgGridlab} shows the required classes to connect \gls{ptl} to \gridlab{}.  The Core component controls the simulation, requesting components synchronise when they are needed.  This is why \gls{ptl} and the simulator needed to be so closely linked.  With core controlling the time synchronisation of method calls, the connecting code needed to force \gls{ptl} and the Residential component to happen together so that the correct sequence of action selection, learning and transfer could happen.  For the \gls{ptl} library to be used it required an interpretor, this converts the actual values for load and battery charge to states.  Additionally, Reward needs to be extended for each objective, so that the agents can determine how well they are doing.  In \gridlab{'s} Residential component, the class representing the particular device we are using (EVCharger is an \gls{ev}) is altered so that it uses \gls{ptl} or \gls{rl}.  There are other classes in the Residential component, but they are not impacted on by integration with \gls{ptl}.

\begin{figure}[hbt!]
\centering
\includegraphics[width=\linewidth,keepaspectratio]{figures/chap6ClassGridlab}
\caption[The Relationship between \acrlong{ptl} and \gridlab{}]{The Relationship between \acrlong{ptl} and \gridlab{}.}
\label{6imgGridlab}
\end{figure}

The \gls{sg} is a new model for electrical grids by which flexibility is introduced through new technologies most notably computing and intelligent control~\citep{farhangi2010path}.  Since the first power grids in the late \nth{19} century the electrical grid has operated one way, from large central power plants to end-users.  Demand was reasonably stable and the main concern for grid operators was providing economical, reliable power.  In the early \nth{21} century several changes began happening that have affected the problem of control in the electrical grid~\citep{fang2012smart}:  
\begin{itemize}
\item \textbf{Distributed Generation} changes the way electricity flows in the grid, coming from end-users rather than just to them.
\item \textbf{Electric Vehicles} not only increase demand (as energy comes from the grid rather than internal combustion), but provide capacity for distributed energy storage.
\item \textbf{Abundant Sensors} make it easier to know the current state of the grid and inform predictions about future states.
\item \textbf{Renewable Generation} is generally less flexible than conventional generation, so the grid must accommodate this.  For example, the output of an oil fired power plant can be easily increased or decreased, but a wind turbine's output can only be decreased by its operators.
%DONE CONTENT other items that change in Smart Grid
\end{itemize}
While conventional control of the grid could accommodate these things, the \gls{sg}['s] aim is to improve the efficiency of the grid, which would be curtailed by inexact control schemes.  Aside from the variability in the grid, the number of different stakeholders means each device can have multiple different objectives to be balanced.  These factors make the \gls{sg} an interesting venue for applying learning-based control.

\gls{dr} is a method of solving many problems in \gls{sg} applications~\citep{mohsenian2010autonomous,forouzandehmehr2015autonomous}.  Rather than as was traditionally done, increasing electricity generation to match demand, in \gls{dr} electricity demand is increased or decreased to equal generation.  There are several reasons why this approach is preferable to supply side methods:
\begin{itemize}
\item \textbf{Ancillary Services} are electrical concerns (such as maintaining correct frequency) that can impact on the quality of the electricity produced.  There can be rapid and dynamic changes that must be addressed in real time.  A central solution can take time to propagate to their location and affect other areas of the grid as well.  
\item \textbf{Uncontrollable Supply} is generally from renewable energy and can lead to over production of electricity.  This surplus would otherwise be wasted.  The disposal of this excess energy is known as curtailment and it can be expensive.  Renewable energy sometimes has to be curtailed because of the guarantees of energy quality provided, which can not be maintained by ancillary services otherwise~\citep{harris2014distributed}
\item The \textbf{Cost} of meeting high demand peaks is generally significant, as markets buy the cheapest energy first.  If the peak can be shifted to some other time more cheaper energy can be bought.
\item \textbf{Grid Resilience} can be achieved through \gls{dr}'s load shedding.  If there is some unpredictable event increasing demand or lowering supply, a reduction in load can protect grid stability.
\item \textbf{User Benefits} are potentially available in energy bill reduction.  As there is considerable benefit to the grid there will be cost incentives to participate in such programs.
\item \textbf{Hardware Life} can be prolonged by reducing the amount of times a device is pushed close to its limit and by smoothing demand profiles.
\end{itemize}

To implement \gls{dr}, devices must be flexible and able to control their own operation.  When demand is less than supply, extra demand can be introduced either by activating more devices (for example, turning on storage devices) or increasing demand in currently operating ones (for example, charging \glspl{ev} at a higher rate).  If supply is less than demand, the demand can be shed though the opposite operations.  

The degree to which individual devices can contribute to \gls{dr} programs depends on their operational requirements and electricity draw.  Devices with an expectation of timeliness (ovens for example), lack the required flexibility to be useful.  Devices with a small power draw do not have a significant impact.  Devices with large power requirements and little or no energy storage (such as clothes dryers) are really only useful for adding demand.  Devices with storage and large power draws that are both flexible and have an effect on the grid are most useful for delivering \gls{dr} (for example, \glspl{ev}, heating systems, water heaters, etc.).  The storage can be thermal, chemical, electrical or any other form, all that is required is that operation can be rescheduled within some time window without affecting users overly.

\gls{dr} tends to be referred to using the specific terminology depending on whether load is being raised or lowered.  Lowering demand is called peak reduction or peak shaving, raising demand is called valley filling.  Both of these can be achieved independently or as a result of load shifting.  Figure~\ref{6imgDR} shows a sample of normal residential demand over one day.  Typically residential energy use follows this pattern, low usage overnight and in the early morning, followed by higher usage until the afternoon, before peaking in the evening.  Ideally this evening peak would be reduced and the night time valley filled, achieving load shifting and flattening the demand profile.  This demand profile without the reschedulable load is commonly called base load.  Generally, the base load and the well scheduled reschedulable load together will produce a flat line, as a stable level of output is preferable for large-scale generation.  Other shapes can be produced if \gls{dr} has other goals (e.g., to maximise renewable energy generation the shape will correspond to the level of generated energy).  

From a \gls{rl} point of view, the \gls{sg} is non-Markovian.  While a single-agent version would be Markovian, the ambiguity of other agents introduce the possibility of temporal dependencies.  The simple Markovian `test' of would a prediction be improved by knowledge of previous time steps, in this case would be failed.  Knowledge of what other agents do and how they decide on it would greatly improve prediction.
\begin{figure}[hbt!]
\centering
\includegraphics[width=.5\linewidth,keepaspectratio]{figures/chap6DR}
\caption[A Single Day's Electricity Usage]{A Single Day's Electricity Usage.}
\label{6imgDR}
\end{figure}