forThesis=1;

slowLearn=log(1:800)-5;
fastLearn=(log(log(1:800)));



%set style for printing
fig1=figure();
box on
set(gca,'FontSize',14);
titleText=sprintf('Benefits of Accelerated Learning');
title(titleText,'fontweight','bold')
xlabel('Time') % x-axis label
set(gca,'xtick',[])
ylabel('Performance') % y-axis label
set(gca,'ytick',[])
set(gca,'ylim',[min(slowLearn) max(fastLearn)*1.2])
if forThesis==1
    set(fig1, 'Position', [0 0 1000 200])
else
    set(fig1, 'Position', [0 0 500 500])
end
lineStyle={'-.'; '-'; '--'; '-.'};
lineWeight={1;1;1;1};
lineColour={'r';[0 0.7 0];'b';'k'};
lineMarker={'x';'.';'*';'o'};

hold on
%plot some things
plotStyleIndex=1;
plot(fastLearn,'linestyle',cell2mat(lineStyle(plotStyleIndex,1)),'color',cell2mat(lineColour(plotStyleIndex,1)),'LineWidth',cell2mat(lineWeight(plotStyleIndex,1)))
plotStyleIndex=plotStyleIndex+1;
plot(slowLearn,'linestyle',cell2mat(lineStyle(plotStyleIndex,1)),'color',cell2mat(lineColour(plotStyleIndex,1)),'LineWidth',cell2mat(lineWeight(plotStyleIndex,1)))

legend('Fast Learning','Slow Learning','dummy 1','dummy 2','Location','southeast')
hold off
%xlim(min, max)
%ylim(min,max)
ti = get(gca,'TightInset')%trim white space and paper
set(gca,'Position',[ti(1) ti(2) 1-ti(3)-ti(1) 1-ti(4)-ti(2)]);
set(gca,'units','centimeters')
pos = get(gca,'Position');
ti = get(gca,'TightInset');
set(gcf, 'PaperUnits','centimeters');
set(gcf, 'PaperSize', [pos(3)+ti(1)+ti(3) pos(4)+ti(2)+ti(4)]);
set(gcf, 'PaperPositionMode', 'manual');
set(gcf, 'PaperPosition',[0 0 pos(3)+ti(1)+ti(3) pos(4)+ti(2)+ti(4)]);

if forThesis==1
    outname=sprintf('C:\\Users\\Adam\\Dropbox\\reports\\thesis!!!!!!!!\\figures\\chap2Perf.pdf')
    print(fig1,'-painters', '-noui','-dpdf',outname)%'-r5500'
else
    outname=sprintf('C:\\Users\\Adam\\Dropbox\\reports\\thesis!!!!!!!!\\presentation\\chap2Perf.png')
    print(fig1,'-painters', '-noui','-r1500','-dpng',outname)%
end
