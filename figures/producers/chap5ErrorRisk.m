mean= 325;
samples=[324.68,324.73,324.35,325.35,325.23,324.13,324.53,325.23,324.6,324.63,325.15,326.33,327.25,325.83,326.5,324.68,327.78,326.88,328.35]
shi=zeros(1,length(samples));
for index=2:length(samples)
shi(index)=max(0,shi(index-1)+samples(index)-mean-.3175);
end
slo=max(0,-samples+mean-.3175);
h=zeros(1,length(samples));
h(:)=4.1959;
%set style for printing
fig1=figure();
lineStyle={'-'; '-'; '--'; '-.'};
lineWeight={1;1;1;1};
lineColour={'b';[0 0.9 0];'b';'k'};
lineMarker={'x';'x';'*';'o'};
hold on

%plot some things
plotStyleIndex=1;
plot(h(1:19),'linestyle',cell2mat(lineStyle(plotStyleIndex,1)),'color',cell2mat(lineColour(plotStyleIndex,1)),'LineWidth',cell2mat(lineWeight(plotStyleIndex,1)))
plotStyleIndex=plotStyleIndex+1;
plot(shi(1:19),'linestyle',cell2mat(lineStyle(plotStyleIndex,1)),'color',cell2mat(lineColour(plotStyleIndex,1)),'LineWidth',cell2mat(lineWeight(plotStyleIndex,1)),'Marker',cell2mat(lineMarker(plotStyleIndex,1)))
plotStyleIndex=plotStyleIndex+1;
%plot(slo,'linestyle',cell2mat(lineStyle(plotStyleIndex,1)),'color',cell2mat(lineColour(plotStyleIndex,1)),'LineWidth',cell2mat(lineWeight(plotStyleIndex,1)))

%set graph area
box on
legend('h','S_{hi}','dummy 1','dummy 2','Location','northwest')
set(gca,'FontSize',20)
title('The S_{hi} Bounds of the Process')
xlabel('Time') % x-axis label
%set(gca,'xtick',[])
ylabel('S_{hi} Score') % y-axis label
%set(gca,'ytick',[])
xlim([1, length(samples)])
ylim([0,12])
NumTicks = 6;
L = get(gca,'YLim');
%set(gca,'YTick',linspace(L(1),L(2),NumTicks))
outname=sprintf('C:\\Users\\Adam\\Dropbox\\reports\\thesis!!!!!!!!\\figures\\chap5ErrorRisk.pdf')
ti = get(gca,'TightInset')%trim white space and paper
set(gca,'Position',[ti(1) ti(2) 1-ti(3)-ti(1) 1-ti(4)-ti(2)]);
set(gca,'units','centimeters')
pos = get(gca,'Position');
ti = get(gca,'TightInset');
set(gcf, 'PaperUnits','centimeters');
set(gcf, 'PaperSize', [pos(3)+ti(1)+ti(3) pos(4)+ti(2)+ti(4)]);
set(gcf, 'PaperPositionMode', 'manual');
set(gcf, 'PaperPosition',[0 0 pos(3)+ti(1)+ti(3) pos(4)+ti(2)+ti(4)]);

print(fig1,'-painters', '-noui','-dpdf','-r5500',outname)
