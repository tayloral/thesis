slowLearn=log(1:800)-5;
fastLearn=(log(log(1:800)));



%set style for printing
fig1=figure();
lineStyle={'-.'; '-'; '--'; '-.'};
lineWeight={1;1;1;1};
lineColour={'r';[0 0.9 0];'b';'k'};
lineMarker={'x';'.';'*';'o'};
hold on

%plot some things
plotStyleIndex=1;
plot(slowLearn,'linestyle',cell2mat(lineStyle(plotStyleIndex,1)),'color',cell2mat(lineColour(plotStyleIndex,1)),'LineWidth',cell2mat(lineWeight(plotStyleIndex,1)))
plotStyleIndex=plotStyleIndex+1;
plot(fastLearn,'linestyle',cell2mat(lineStyle(plotStyleIndex,1)),'color',cell2mat(lineColour(plotStyleIndex,1)),'LineWidth',cell2mat(lineWeight(plotStyleIndex,1)))

%set graph area
box on
legend('Slow Learning','Fast Learning','dummy 1','dummy 2','Location','best')
set(gca,'FontSize',12)
title('The Benefit of Accelerated Learning')
xlabel('Time') % x-axis label
set(gca,'xtick',[])
ylabel('Performance') % y-axis label
set(gca,'ytick',[])
%xlim(min, max)
%ylim(min,max)
outname=sprintf('C:\\Users\\Adam\\Dropbox\\reports\\thesis!!!!!!!!\\figures\\chap2Perf.pdf')
ti = get(gca,'TightInset')%trim white space and paper
set(gca,'Position',[ti(1) ti(2) 1-ti(3)-ti(1) 1-ti(4)-ti(2)]);
set(gca,'units','centimeters')
pos = get(gca,'Position');
ti = get(gca,'TightInset');
set(gcf, 'PaperUnits','centimeters');
set(gcf, 'PaperSize', [pos(3)+ti(1)+ti(3) pos(4)+ti(2)+ti(4)]);
set(gcf, 'PaperPositionMode', 'manual');
set(gcf, 'PaperPosition',[0 0 pos(3)+ti(1)+ti(3) pos(4)+ti(2)+ti(4)]);

print(fig1,'-painters', '-noui','-dpdf','-r5500',outname)
