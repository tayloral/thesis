start=1;%where to plot from
stop=1000;%1 for plot all
axisCount=10;%how many labels to have
printing=1;%1 is to print
%read the file
filename=sprintf('C:\\Users\\CakeBox\\Documents\\ADAM_CODE\\TrunkLast\\VS2005\\x64\\Debug\\V_Data_transformer.csv');
fid = fopen(filename,'r');
data = textscan(fid, repmat('%s',1,10), 'delimiter','\t', 'CollectOutput',true);
fclose(fid);
%clean data
data=data{1};
dates=data(:,1);
power=data(:,3);
power(1:9,:)=[];%clear inital crap
dates(1:9,:)=[];
power=str2double(power);
if stop==1
    stop=length(power)-1;
end
%add any extra series here then set them to plot



%set style for printing
fig1=figure();
lineStyle={'-.'; '-'; '--'; '-.'};
lineWeight={1;1;1;1};
lineColour={'r';[0 0.9 0];'b';'k'};
lineMarker={'x';'.';'*';'o'};
hold on

%plot some things
plotStyleIndex=1;
plot(power(start:stop),'linestyle',cell2mat(lineStyle(plotStyleIndex,1)),'color',cell2mat(lineColour(plotStyleIndex,1)),'LineWidth',cell2mat(lineWeight(plotStyleIndex,1)))
plotStyleIndex=plotStyleIndex+1;
%plot(fastLearn,'linestyle',cell2mat(lineStyle(plotStyleIndex,1)),'color',cell2mat(lineColour(plotStyleIndex,1)),'LineWidth',cell2mat(lineWeight(plotStyleIndex,1)))

%set graph area
box on
legend('Slow Learning','Fast Learning','dummy 1','dummy 2','Location','best')
set(gca,'FontSize',12)
title('Transformer Load')
xlabel('Time') % x-axis label
axisSpace=(stop-start)/(axisCount-1);%
set(gca,'xtick',0:axisSpace:(stop-start))%change axis
set(gca,'XTickLabel',dates(start:stop));
rotateXLabels( gca(), 45 );
ylabel('Performance') % y-axis label
%set(gca,'ytick',[])%change axis
%xlim(min, max)
%ylim(min,max)

if printing==1
    set(fig1, 'visible','off');
    set(fig1, 'Position', [0 0 1000 1000]);
    outname=sprintf('C:\\Users\\CakeBox\\Documents\\ADAM_CODE\\TrunkLast\\VS2005\\x64\\Debug\\output.pdf')
    ti = get(gca,'TightInset');%trim white space and paper
    set(gca,'Position',[ti(1) ti(2) 1-ti(3)-ti(1) 1-ti(4)-ti(2)]);
    set(gca,'units','centimeters')
    pos = get(gca,'Position');
    ti = get(gca,'TightInset');
    set(gcf, 'PaperUnits','centimeters');
    set(gcf, 'PaperSize', [pos(3)+ti(1)+ti(3) pos(4)+ti(2)+ti(4)]);
    set(gcf, 'PaperPositionMode', 'manual');
    set(gcf, 'PaperPosition',[0 0 pos(3)+ti(1)+ti(3) pos(4)+ti(2)+ti(4)]);
    print(fig1,'-painters', '-noui','-dpdf','-r5500',outname)%print it
end